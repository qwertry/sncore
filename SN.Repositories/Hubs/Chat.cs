﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.SignalR;
using SN.Domains.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Infrastructure
{

    public class Chat : Hub
    {

        private IUserIdProvider _userIdProvider;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        private readonly IHttpContextAccessor _httpContextAccessor;

        public static Dictionary<Guid, string> UserIdsSocketId = new Dictionary<Guid, string>();

        public Chat(IUserIdProvider userIdProvider,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _signInManager = signInManager;
            _userIdProvider = userIdProvider;
            _userManager = userManager;
        }

        public Task SendPrivateMessage(string user, string message)
        {

            return Clients.User(user).SendAsync("ReceiveMessage", message);
        }

        public async Task SendMessage(string user, string message)
        {

            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        public override Task OnConnectedAsync()
        {
            try
            {

                var context = _httpContextAccessor.HttpContext;

                var currentUser = _userManager.GetUserAsync(context.User).Result;

                Guid ownerId = currentUser.Id;

                lock (UserIdsSocketId)
                {
                    if (UserIdsSocketId.ContainsKey(ownerId))
                    {
                        UserIdsSocketId[ownerId] = Context.ConnectionId;
                    }
                    else
                    {
                        UserIdsSocketId.Add(ownerId, Context.ConnectionId);
                    }
                }

                return base.OnConnectedAsync();
            }
            catch(Exception ex)
            {
                var l = ex;
                Task completedTask = Task.CompletedTask;
                return completedTask;
            }

        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            if (!Chat.UserIdsSocketId.Any(u => u.Value.Equals(Context.ConnectionId)))
            {
                return base.OnDisconnectedAsync(exception);
            }
            var usertoken = Chat.UserIdsSocketId.SingleOrDefault(u => u.Value.Equals(Context.ConnectionId));
            lock (UserIdsSocketId)
            {
                UserIdsSocketId.Remove(usertoken.Key);
            }
            return base.OnDisconnectedAsync(exception);
        }


    }
}
