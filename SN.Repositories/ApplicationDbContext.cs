﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SN.Domains.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Infrastructure
{
    public class ApplicationDbContext
    : IdentityDbContext<ApplicationUser, IdentityRole<Guid>, Guid>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            Database.Migrate();

        }
        public DbSet<Post> Post { get; set; }
        public DbSet<FriendRequest> FriendRequest { get; set; }
        public DbSet<Friend> Friend { get; set; }
        public DbSet<Interest> Interests { get; set; }
        public DbSet<UserInterest> UserInterests { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<MessageSession> MessageSessions { get; set; }
        public DbSet<PostComment> PostComments { get; set; }
        public DbSet<PermitedUser> PermitedUsers { get; set; }
        public DbSet<Image> Images { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<PermitedUser>()
            .HasOne(ms => ms.Post)
            .WithMany(b => b.PermitedUsers)
            .HasForeignKey(p => p.PostId)
            .HasPrincipalKey(p => p.PostId);

            builder.Entity<PostComment>()
            .HasOne(ms => ms.Post)
            .WithMany(b => b.Comments)
            .HasForeignKey(p => p.PostId)
            .HasPrincipalKey(p => p.PostId);

            builder.Entity<MessageSession>()
            .HasOne(ms => ms.ApplicationUser)
            .WithMany(b => b.MessageSessions)
            .HasForeignKey(p => p.ApplicationUserId)
            .HasPrincipalKey(p => p.Id);

            builder.Entity<Message>()
            .HasOne(ms => ms.MessageSession)
            .WithMany(b => b.Messages)
            .HasForeignKey(p => p.MessageSessionId)
            .HasPrincipalKey(p => p.MessageSessionId);

            builder.Entity<Post>()
            .HasOne(p => p.ApplicationUser)
            .WithMany(b => b.Posts)
            .HasForeignKey(p => p.ApplicationUserId)
            .HasPrincipalKey(p => p.Id);


            builder.Entity<Friend>()
                .HasOne(f => f.UserFriend)
                .WithMany(f => f.Friends)
                .HasForeignKey(s => s.UserFriendId)
                .HasPrincipalKey(p => p.Id)
                    .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<FriendRequest>()
                .HasOne(fr => fr.Sender)
                .WithMany(fr => fr.FriendRequests)
                .HasForeignKey(s => s.SenderId)
                .HasPrincipalKey(p => p.Id)
                    .OnDelete(DeleteBehavior.Cascade);

            

            builder.Entity<UserInterest>()
                .HasOne<ApplicationUser>(sc => sc.AppicationUser)
                .WithMany(s => s.UserInterests)
                .HasForeignKey(sc => sc.AppicationUserId)
                .HasPrincipalKey(p => p.Id);


            builder.Entity<UserInterest>()
                .HasOne<Interest>(sc => sc.Interest)
                .WithMany(s => s.UserInterests)
                .HasForeignKey(sc => sc.InterestId)
                .HasPrincipalKey(p => p.InterestId);

        }

    }


}
