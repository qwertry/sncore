﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using SN.Domains.Entities;
using SN.Domains.Enums;
using SN.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Infrastructure
{
    public class SeedDatabase
    {
        public static async void Initialize(IServiceProvider serviceProvider)
        {
            
            var context = serviceProvider.GetRequiredService<ApplicationDbContext>();

            context.Database.EnsureCreated();

            await CreateRolesAsync(serviceProvider);

            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            
            if (!context.Users.Any())
            {

                var administrator = new ApplicationUser()
                {
                    Email = "admin@admin.admin",
                    UserName = "Admin"
                };
                await userManager.CreateAsync(administrator, "qwQW12!@");
                await userManager.AddToRoleAsync(administrator, "Administrator");

                ApplicationUser[] users = new ApplicationUser[200];
                for (int i = 0; i < 200; i++)
                {
                    var user = new ApplicationUser()
                    {
                        Email = "user" + i + "@first.first",
                        SecurityStamp = Guid.NewGuid().ToString(),
                        UserName = "User" + i
                    };
                    var result = await userManager.CreateAsync(user, "qwQW12!@");
                    await userManager.AddToRoleAsync(user, "Member");
                    users[i] = user;
                    if (i % 10 == 0 && i!=0 )
                    {
                        var permitedUsers = new List<PermitedUser>();
                        var date = DateTime.UtcNow;
                        for (int j = i-1; i-9 < j ; j--)
                        {
                            await context.Friend.AddRangeAsync(new Friend()
                            {
                                UserId = users[i].Id,
                                UserFriendId = users[j].Id,
                                FriendshipStrartTime = date
                            },
                            new Friend()
                            {
                                UserId = users[j].Id,
                                UserFriendId = users[i].Id,
                                FriendshipStrartTime = date
                            }
                            );
                            if (j > i - 5)
                            {
                                permitedUsers.Add(new PermitedUser() {
                                    ApplicationUserId = users[j].Id
                                });
                            }
                        }
                        PostPermissionTypes pType;
                        if (i % 20==0)
                        {
                            pType = PostPermissionTypes.AllFriendsExeptSomeFriends;
                        }
                        else
                        {
                            pType = PostPermissionTypes.OnlySomeFriends;
                        }

                        context.Post.Add(new Post()
                        {
                            ApplicationUserId = users[i].Id,
                            AliasName=i,
                            PostedAt = date,
                            Body = "Post bla bla bla",
                            PostPermissionType = pType,
                            PermitedUsers = permitedUsers
                        });
                    }
                }

                
                context.SaveChanges();
            }

        }

        private async static Task CreateRolesAsync(IServiceProvider serviceProvider)
        {

            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole<Guid>>>();
            
            List<string> roles = new List<string>() {
                    "Administrator",
                    "Manager",
                    "Member"
                };

            foreach (string role in roles)
            {
                if (await roleManager.RoleExistsAsync(role))
                {
                    continue;
                }
                await roleManager.CreateAsync(new IdentityRole<Guid>(role));
            }
        }
    }
}

