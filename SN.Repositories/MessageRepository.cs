﻿using Microsoft.EntityFrameworkCore;
using SN.Domains.Entities;
using SN.Domains.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SN.Domains;
using SN.BuisnessLogic.Repositories;

namespace SN.Infrastructure
{
    public class MessageRepository : BaseRepository<Message>, IMessageRepository
    {
        protected readonly ApplicationDbContext _context;
        public MessageRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
            this._context = dbContext;
        }

        public Task<bool> ChangeMessageStatus(Message message, MessageStatus newStatus)
        {
            throw new NotImplementedException();
        }

        public Message CreateMessageForSaving(Guid messageSessionId, string messageBody, Guid senderId, Guid recieverId, MessageStatus status)
        {
            return new  Message()
            {
                MessageSessionId = messageSessionId,
                MessageBody = messageBody,
                SenderId = senderId,
                RecieverId = recieverId,
                SendingDate = DateTime.UtcNow,
                Status = status
            };
        }

        public Task<Message> FindMessage(Guid messageId)
        {
            throw new NotImplementedException();
        }

        public async Task<IQueryable<Message>> GetMessages(Guid messagSessionId, int from, int to)
        {
            int limit =to - from + 1;

            //string query = ($"SELECT * FROM Messages WHERE MessageSessionId =  {messagSessionId} ORDER BY SendingDate DESC OFFSET {from} ROWS FETCH NEXT {limit} ROWS ONLY");

            //var resultsSQL = _context.Messages.FromSql(query);
            var results = _context.Messages.Skip(from).Take(limit).OrderByDescending(m=>m.MessageId==messagSessionId);
            return results;
        }
    }
}
