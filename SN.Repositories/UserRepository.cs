﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SN.Domains.Entities;
using SN.Domains;
using Microsoft.AspNetCore.Identity;
using SN.Domains.Models.Inerfaces;
using SN.BuisnessLogic.Repositories;

namespace SN.Infrastructure
{
    public class UserRepository : BaseRepository<ApplicationUser>, IUserRepository
    {
        protected readonly ApplicationDbContext _context;
        protected readonly UserManager<ApplicationUser> _userManager;
        public UserRepository(ApplicationDbContext dbContext, UserManager<ApplicationUser> userManager)
            : base(dbContext)
        {
            this._context = dbContext;
            _userManager = userManager;
        }

        public async Task<IdentityResult> AddToRoleAsync(ApplicationUser user, string role)
        {
            return await _userManager.AddToRoleAsync(user, role);
        }

        public async Task<IdentityResult> CreateAsync(ApplicationUser user, string password)
        {
            return await _userManager.CreateAsync(user, password);
        }

        public async Task<ApplicationUser> GetUserOrDefaultAsync(ILogable model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                return user;
            }
            return null;
        }
    }
}
