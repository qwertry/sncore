﻿
using SN.Domains.Entities;
using System;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Claims;
using SN.BuisnessLogic.Repositories;

namespace SN.Infrastructure
{
    public class JWTRepository:IJWTRepository
    {
       
        public object CreateToken(ApplicationUser user)
        {
            var claims = new[]
               {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("lalalallalallalalallalallalalallalal"));
            var token = new JwtSecurityToken(
                issuer: "http://oec.com",
                audience: "http://oec.com",
                expires: DateTime.UtcNow.AddHours(1),
                claims: claims,
                signingCredentials: new Microsoft.IdentityModel.Tokens.SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
                );

            return (new
            {
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = token.ValidTo
            });

        }
    }
}
