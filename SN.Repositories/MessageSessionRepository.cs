﻿using Microsoft.EntityFrameworkCore;
using SN.BuisnessLogic.Repositories;
using SN.Domains.Entities;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Infrastructure
{
    public class MessageSessionRepository: BaseRepository<MessageSession>, IMessageSessionRepository
    {
        protected readonly ApplicationDbContext _context;
        public MessageSessionRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
            this._context = dbContext;
        }

        public async Task<MessageSession> CreateMessageSession(Guid applicationUserId, Guid recieverId)
        {
            var messageSessionObject = new MessageSession() { MessageSessionId = Guid.NewGuid(), ApplicationUserId = applicationUserId, RecieverId = recieverId };
            await _context.MessageSessions.AddAsync(messageSessionObject);
            return messageSessionObject;
        }

        public async Task<MessageSession> GetMessageSession(Guid applicationUserId, Guid recieverId)
        {
            return await _context.MessageSessions
                 .SingleOrDefaultAsync(ms =>
                 (ms.ApplicationUserId == applicationUserId && ms.RecieverId == recieverId)
                 || (ms.ApplicationUserId == recieverId && ms.RecieverId == applicationUserId));

        }

        public async Task<IQueryable<MessageSession>> GetMessageSessionsOfUser(Guid applicationUserId)
        {
            return  _context.MessageSessions.Where(ms=>ms.ApplicationUserId==applicationUserId || ms.RecieverId == applicationUserId);
        }

        public async Task<MessageSession> GetMessageSessionById(Guid messageSessionId)
        {
            return await _context.MessageSessions.SingleOrDefaultAsync(ms=>ms.MessageSessionId == messageSessionId);
        }
    }
}
