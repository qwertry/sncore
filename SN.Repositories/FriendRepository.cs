﻿using Microsoft.EntityFrameworkCore;
using SN.Domains.Entities;
using SN.Domains.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SN.Domains;
using SN.Domains.FriendRequest;
using SN.BuisnessLogic.Repositories;

namespace SN.Infrastructure
{
    public class FriendRepository:BaseRepository<Friend>,IFriendRepository
    {
        protected readonly ApplicationDbContext _context;
        public FriendRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
            this._context = dbContext;
        }

        

        public async  Task<IQueryable<FriendRequest>> GetFriendRequestsAsync(Guid requestsOwner)
        {
            return  _context.FriendRequest.Where(r=>r.RecipientId == requestsOwner).Include(f => f.Sender);
        }

        public async Task<IQueryable<Friend>> GetFriends()
        {
            return _context.Friend.OrderBy(c => c.FriendshipStrartTime);
        }

        public async Task<bool> PlaceRequestAsync(Guid from, Guid to)
        {
            var sender = _context.Users.SingleOrDefault(u => u.Id == from);
            var recipient = _context.Users.SingleOrDefault(u => u.Id == to);
            if (sender == null || recipient == null)
            {
                return false;
            }
            
            if (await IsFriendsAsync(from, to))
            {
                return false;
            }

            var friendRequest = await GetFriendRequestAsync(from, to);

            if (friendRequest != null)
            {
                return false;
            }
            var incomingFriendRequest = await GetFriendRequestAsync(to, from);

            if (incomingFriendRequest != null)
            {
                return await ResponceToFriendRequestAsync(from, new ResponceToFriendRequestDto()
                {
                    FromUser = to,
                    Status = FriendRequestStatus.Accepted
                });
            }

            await _context.FriendRequest.AddAsync(new FriendRequest() {
                SenderId = from,
                RecipientId = to,
                SendingDate = DateTime.UtcNow
            });
            return true;
            
        }


        public async Task<bool> ResponceToFriendRequestAsync(Guid responcer, ResponceToFriendRequestDto responce)
        {
            if (responce.Status == FriendRequestStatus.Accepted)
            {
                if (await IsFriendsAsync(responcer, responce.FromUser))
                {
                    return false;
                }
                var friendRequest = await GetFriendRequestAsync(responce.FromUser, responcer);

                if (friendRequest == null)
                {
                    return false;
                }

                await _context.Friend.AddRangeAsync(new Friend() {
                    UserId = responcer,
                    UserFriendId = responce.FromUser,
                    FriendshipStrartTime = DateTime.UtcNow
                },
                new Friend()
                {
                    UserId = responce.FromUser,
                    UserFriendId = responcer,
                    FriendshipStrartTime = DateTime.UtcNow
                }
                );
                

                 _context.FriendRequest.Remove(friendRequest);
                return true;

            }else if(responce.Status == FriendRequestStatus.Denied)
            {
                var friendRequest =
                    await _context.FriendRequest.SingleOrDefaultAsync(fr => (fr.RecipientId == responcer && fr.SenderId == responce.FromUser)
                || (fr.RecipientId == responce.FromUser && fr.SenderId == responcer));
                if (friendRequest == null)
                {
                    return false;
                }
                _context.FriendRequest.Remove(friendRequest);
                return true;
            }
            return false;
        }

        public async Task<FriendRequest> GetFriendRequestAsync(Guid from, Guid to)
        {
            return await _context.FriendRequest.SingleOrDefaultAsync(r => r.SenderId == from && r.RecipientId == to);
        }

        public async Task<IQueryable<Friend>> GetAllFriendsAsync(Guid userId)
        {
            return _context.Friend.Where(f => f.UserId == userId).Include(f => f.UserFriend);
        }

        public async Task RemoveFriendRequestAsync(FriendRequest request)
        {
            _context.FriendRequest.Remove(request);
        }

       public async Task<bool> IsFriendsAsync(Guid userId, Guid friendId)
        {
            return await _context.Friend.AnyAsync(f =>
           (f.UserId == userId && f.UserFriendId == friendId)
           || (f.UserId == friendId && f.UserFriendId == userId)
            );
        }

        public async Task DestroyFrindshipAsync(Guid userId, Guid friendId)
        {
            var friends =  _context.Friend.Where(f => (f.UserId == userId && f.UserFriendId == friendId) || (f.UserId == friendId && f.UserFriendId == userId) );
            if(await friends?.AnyAsync())
            {
                await RemoveAsync(await friends.ToArrayAsync());
            }
        }
    }
}
