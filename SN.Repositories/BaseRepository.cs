﻿using Microsoft.EntityFrameworkCore;
using SN.Domains.Entities;
using SN.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SN.BuisnessLogic.Repositories;

namespace SN.Infrastructure
{
    public abstract class BaseRepository<T>:IRepository<T> where T : class
    {
        protected readonly ApplicationDbContext dbContext;

        public BaseRepository(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public virtual async Task AddAsync(T newEntity)
        {
            await dbContext.AddAsync(newEntity);
        }

        public virtual async Task UpdateAsync(T updatableEntity)
        {
            dbContext.Update(updatableEntity);
        }

        public virtual async Task RemoveAsync(params T[] entity)
        {
            dbContext.RemoveRange(entity);
        }
        
        public virtual async Task<bool> RecordExistsAsync(Guid recordId)
        {
            var keyProperty = dbContext.Model.FindEntityType(typeof(T)).FindPrimaryKey().Properties[0];
            return await dbContext.Set<T>().AnyAsync(e => EF.Property<Guid>(e, keyProperty.Name) == recordId);
        }

        public async Task<IQueryable<T>> FindAsync(Expression<Func<T, bool>> predicate)
        {
            var obj = dbContext.Set<T>().Where(predicate);
            if (!obj.Any())
                return null;
            return obj;
        }

        public async Task<IQueryable<T>> GetAllAsync()
        {
            return dbContext.Set<T>();
        }
        
        public virtual async Task<T> GetByIdAsync(Guid recordId)
        {
            var keyProperty = dbContext.Model.FindEntityType(typeof(T)).FindPrimaryKey().Properties[0];
            return await dbContext.Set<T>().SingleOrDefaultAsync(e => EF.Property<Guid>(e, keyProperty.Name) == recordId);
        }

        public virtual async Task<bool> SaveAsync()
        {
            return (await dbContext.SaveChangesAsync() >= 0);
        }

    }
}
