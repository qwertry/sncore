﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using SN.BuisnessLogic.Repositories;
using SN.Domains.Entities;
using SN.Domains.Models.ReturnDtos.MessageDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.Infrastructure
{
    public class HubRepository : IHubRepository
    {
        private IHubContext<Chat> _messageHubContext;
        public HubRepository(IHubContext<Chat> messageHubContext)
        {
            _messageHubContext = messageHubContext;
        }
        public  async Task SendMessageAsync(Message messageForSend)
        {
            if (Chat.UserIdsSocketId.Any(u => u.Key == messageForSend.RecieverId))
            {
                var messageToSocket = Mapper.Map<MessageDto>(messageForSend);

                var user = Chat.UserIdsSocketId.SingleOrDefault(u => u.Key == messageToSocket.RecieverId);
                await _messageHubContext.Clients.Client(user.Value).SendAsync("send", messageToSocket);
            }

        }
    }
}
