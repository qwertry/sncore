﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SN.Domains.Entities;
using Microsoft.EntityFrameworkCore;
using SN.Domains.Enums;
using SN.Domains;
using SN.Domains.Models.RequestDto.Posts;
using SN.BuisnessLogic.Repositories;

namespace SN.Infrastructure
{
    public class PostRepository : BaseRepository<Post>, IPostRepository
    {
        protected readonly ApplicationDbContext _context;
        public PostRepository(ApplicationDbContext dbContext)
            : base(dbContext)
        {
            this._context = dbContext;
        }

        public IEnumerable<Post> GetPosts()
        {
            return _context.Post.OrderBy(c => c.PostedAt).ToList();
        }

        public Post GetPost(int cityId, bool includePointsOfInterest)
        {

            return _context.Post.Where(c => c.AliasName == cityId).FirstOrDefault();
        }


        public async Task AddPostAsync(PostModelDto post, Guid owner)
        {
            var permitedUsersToSave = new List<PermitedUser>();
            if (post.PostPermissionType == PostPermissionTypes.AllFriendsExeptSomeFriends ||
                post.PostPermissionType == PostPermissionTypes.OnlySomeFriends)
            {
                if (post.PermitedUsers != null)
                {
                    if (post.PermitedUsers.Any())
                    {
                        foreach (var pu in post.PermitedUsers)
                        {
                            permitedUsersToSave.Add(new PermitedUser()
                            {
                                ApplicationUserId = pu.UserId
                            });
                        }
                    }
                }
            }
           await  _context.Post.AddAsync(new Post
            {
                AliasName = GetEmptyAliasName(),
                ApplicationUserId = owner,
                Body = post.Body,
                PostedAt = DateTime.Now,
                PostPermissionType = post.PostPermissionType,
                PermitedUsers = permitedUsersToSave
           });

        }
        public int GetEmptyAliasName()
        {
            var finalPost = _context.Post
              .OrderByDescending(x => x.PostedAt)
              .FirstOrDefault();
            if (finalPost == null)
                return 1;
            return finalPost.AliasName + 1;
        }


        public async Task<IQueryable<Post>> GetFriendsPosts(Guid userId)
        {
            var posts = from p in _context.Post
                        join f in _context.Friend.Where(f => f.UserId == userId) on p.ApplicationUserId equals f.UserFriendId
                        orderby p.PostedAt descending
                        select p ;
            if (await posts.AnyAsync())
            {
                return posts;
            }
            return null;
        }
        public async Task<IQueryable<Post>> GetFriendsPostsWithMy (Guid userId)
        { 
            var friendPosts = (from p in _context.Post.Where(p=>
            p.PostPermissionType == PostPermissionTypes.All 
         || p.PostPermissionType == PostPermissionTypes.AllFriends
         || (p.PostPermissionType == PostPermissionTypes.AllFriendsExeptSomeFriends && !(p.PermitedUsers.Any(u => u.ApplicationUserId == userId))) 
         || (p.PostPermissionType == PostPermissionTypes.OnlySomeFriends && (p.PermitedUsers.Any(u => u.ApplicationUserId == userId)))
         
            )
                        join f in _context.Friend.Where(f => f.UserId == userId) 
                        on p.ApplicationUserId equals f.UserFriendId orderby p.PostedAt 
                        select p);
           
            var myPosts = _context.Post.Where(p => p.ApplicationUserId == userId);
            var posts = friendPosts.Union(myPosts).OrderByDescending(p => p.PostedAt);
            if (await posts.AnyAsync())
            {
                return posts;
            }
            return null;
        }

        public async Task<IEnumerable<Post>> ShowPosts()
        {
            return await _context.Post.ToListAsync();
        }

        public async Task<Post> GetPostAsync(Guid userId, int aliasName)
        {
            return await _context.Post.SingleOrDefaultAsync(p => p.ApplicationUserId == userId && p.AliasName == aliasName);
        }

        public async Task UpdatePostAsync(Post postEntity, PostModelDto postDto)
        {
            postEntity.Body = postDto.Body;
            postEntity.PostPermissionType = postDto.PostPermissionType;
            if (postDto.PermitedUsers != null)
            {
                if (postDto.PermitedUsers.Any())
                {
                    var permitedUsersToSave = new List<PermitedUser>();
                    foreach(var user in postDto.PermitedUsers)
                    {
                        permitedUsersToSave.Add(new PermitedUser() { ApplicationUserId = user.UserId });
                    }

                    postEntity.PermitedUsers = permitedUsersToSave;
                }
            }
            await UpdateAsync(postEntity);
        }

        public async Task<bool> IsValid(PostModelDto post, Guid ownerId)
        {
            var friends = _context.Friend.Where(f => f.UserId == ownerId);
            if (!friends.Any())
            {
                return false;
            }
            if (post.PostPermissionType == PostPermissionTypes.AllFriendsExeptSomeFriends ||
                post.PostPermissionType == PostPermissionTypes.OnlySomeFriends)
            {
                if (post.PermitedUsers != null)
                {
                    if (post.PermitedUsers.Any())
                    {
                        foreach (var pu in post.PermitedUsers)
                        {
                            if (!friends.Any(f=>f.UserFriendId==pu.UserId))
                            {
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }
    }
}
