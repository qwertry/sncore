import React, { Component } from 'react';

import NotificationService from '../../services/NotificationService';
import AuthService from '../../services/AuthService';
import { Link } from "react-router-dom";
const Auth = new AuthService();

class Login extends Component {
    constructor() {
        super();
        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.Notification = new NotificationService();
        console.log('Login ctor');
        this.state = {
            message: ''
        };
    }
    componentWillMount() {

        if (Auth.loggedIn()) {

            this.Notification.connectToHub(Auth.getToken());
        }

    }
    handleFormSubmit(e) {
        e.preventDefault();
        Auth.login(this.state.username, this.state.password).then(result => {
            if (result === true) {
                this.props.history.replace('/');
            } else {
                this.setState({
                    message: 'Wrong credentials'
                });
                console.log('Wrong credentials');
            }
        });
    }
    render() {
        return (
            <div className="container">
                <div className="card card-login mx-auto mt-5">
                    <div className="card-header">Login</div>
                    <div className="card-body">
                        <form>
                            <div className="form-group">

                                <p> {this.state.message} </p>
                                <div className="form-label-group">
                                    <input className="form-control"
                                        placeholder="Username goes here..."
                                        name="username"
                                        type="text"
                                        id="username"
                                        required="required"
                                        autoFocus="autoFocus"
                                        onChange={this.handleChange} />
                                    <label htmlFor="username">UserName</label>
                                </div>
                            </div>
                            <div className="form-group">
                                <div className="form-label-group">
                                    <input className="form-control"
                                        placeholder="Password goes here..."
                                        name="password"
                                        type="password"
                                        id="password"
                                        required="required"
                                        onChange={this.handleChange} />
                                    <label htmlFor="password">Password</label>
                                </div>
                            </div>

                            <button type="submit" className="btn btn-primary btn-block" onClick={this.handleFormSubmit}>Login</button>
                        </form>
                        <div className="text-center">
                            <Link to="/register" className="d-block small mt-3" href="register.html">Register an Account</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    handleChange(e) {
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    }
}

export default Login;