import React, { Component } from 'react';
import axios from 'axios';
import AuthService from '../../services/AuthService';

const Auth = new AuthService();

class Timeline extends Component {
    constructor() {
        super();
        console.log('Home ctor');
    }
    testRequest() {
        axios.get(AuthService.domain + '/posts/FriendsPosts', AuthService.config).then(res => console.log(res));
    }
    render() {

        return (
            <div className="App">
                {Auth.getOwnerId()}
                <div className="App-header">
                    <img src="" className="App-logo" alt="logo" />
                    <input
                        value="SUBMIT"
                        type="submit"
                        onClick={this.testRequest}
                    />
                </div>
            </div>
        );
    }
}
export default Timeline;