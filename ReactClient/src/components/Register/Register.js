import React, { Component } from 'react';
import AuthService from '../../services/AuthService';
import { Link } from "react-router-dom";
const Auth = new AuthService();
class Register extends Component {
    constructor() {
        super();
        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
        this.handlePasswordMissmatch = this.handlePasswordMissmatch.bind(this);
        this.state = 
            {
                password: '',
                confirmPassword:''
            };
    }
    componentWillMount() {
        if (Auth.loggedIn()) {
            this.props.history.replace('/');
        }
        
    }
    handleFormSubmit(e) {
        e.preventDefault();
        Auth.register(this.state.userName, this.state.password, this.state.email, this.state.firstName, this.state.secondName)
            .then(res => {
                this.props.history.replace('/');
            })
            .catch(err => {
                alert(err);
            })
    }
    handlePasswordMissmatch() {
        console.log(this.state.password);
        if (this.state.password !== this.state.confirmPassword) {
            this.setState({
                passwordsMissMatch: 'Passwords needs to be equal.'
            });
        }
    }
    render() {
        return (
            <div>

                <div className="container">
                    <div className="card card-register mx-auto mt-5">
                        <div className="card-header">Register an Account</div>
                        <div className="card-body">
                            <form>
                                <div className="form-group">
                                    <div className="form-row">
                                        <div className="col-md-6">
                                            <div className="form-label-group">
                                                <input onChange={this.handleChange} type="text" id="firstName" className="form-control" placeholder="First name" name="firstName" required="required" autoFocus="autoFocus" />
                                                <label htmlFor="firstName">First name</label>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-label-group">
                                                <input onChange={this.handleChange} type="text" id="lastName" className="form-control" placeholder="Last name" name="secondName" required="required" />
                                                <label htmlFor="lastName">Last name</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="form-label-group">
                                        <input onChange={this.handleChange} type="email" id="inputEmail" className="form-control" placeholder="Email address" name="email" required="required" />
                                        <label htmlFor="inputEmail">Email address</label>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="form-label-group">
                                        <input onChange={this.handleChange} type="email" id="userName" className="form-control" placeholder="Email address" name="userName" required="required" />
                                        <label htmlFor="userName">User Name</label>
                                    </div>
                                </div>
                                <div className="form-group">
                                    <div className="form-row">
                                        <p> {this.state.passwordsMissMatch} </p>
                                        <div className="col-md-6">
                                            <div className="form-label-group">
                                                <input onChange={this.handleChange, this.handlePasswordMissmatch} type="password" id="inputPassword" name="password" className="form-control" placeholder="Password" required="required" />
                                                <label htmlFor="inputPassword">Password</label>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="form-label-group">
                                                <input onChange={this.handleChange, this.handlePasswordMissmatch} type="password" name="confirmPassword" id="confirmPassword" className="form-control" placeholder="Confirm password" required="required" />
                                                <label htmlFor="confirmPassword">Confirm password</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a className="btn btn-primary btn-block" onClick={this.handleFormSubmit}>Register</a>
                            </form>
                            <div className="text-center">
                                <Link to="/login" className="d-block small mt-3" href="login.html">Login Page</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    handleChange(e) {
        console.log('called handlechange' + this.state.password + this.state.confirmPassword);
        this.setState(
            {
                [e.target.name]: e.target.value
            }
        )
    }
}

export default Register;