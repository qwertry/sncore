import React, { Component } from 'react';
import {  Link } from "react-router-dom";

import AuthService from '../../services/AuthService';
const Auth = new AuthService();
class Header extends Component {
    constructor() {
        super();
        console.log('Header ctor');
    }

    render() {
        return (
            <div>
        
                {Auth.loggedIn() ? (
                    <ul>
                            <li>
                                <Link to="/timeline">Timeline</Link>
                            </li>
                            <li>
                                <button type="button" className="form-submit" onClick={this.Logout}>Logout</button>
                        </li>
                    </ul >


                ) : (
                        <div></div>

                    )}
                </div>
            
        );
    }

    Logout() {
        console.log('logout');
        Auth.logout();
        window.location.href = '/login';
    }

}
export default Header;