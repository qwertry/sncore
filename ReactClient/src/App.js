import React from "react";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

import Login from './components/Login/Login';
import Timeline from './components/Timeline/Timeline';
import Register from './components/Register/Register';
import Header from "./components/Header/Header";
import './styles/bootstrap.min.css';
import './styles/sb-admin.css';
import AuthService from "./services/AuthService";
console.log('from App');
const Auth = new AuthService();

const App = () => {
    document.body.classList.add('bg-dark');
    return (
        <Router>
            <div>
                <Header />
                {Auth.loggedIn() ? (
                    <Route path="/" exact component={Timeline} />
                ) : (
                        <Route path="/" exact component={Login} />
                    )}
                <ClosedRoteForLoggedInUser path="/login" component={Login} />
                <ClosedRoteForLoggedInUser path="/register" component={Register} />
                <PrivateRoute path="/timeline" component={Timeline} />
            </div>
        </Router>
    );
}




function PrivateRoute({ component: Component, ...rest }) {

    return (
        <Route
            {...rest}
            render={props =>

                Auth.loggedIn() ? (
                    <Component {...props} />
                ) : (
                        <Redirect
                            to={{
                                pathname: "/login",
                                state: { from: props.location }
                            }}
                        />
                    )
            }
        />
    );
}

function ClosedRoteForLoggedInUser({ component: Component, ...rest }) {

    return (
        <Route
            {...rest}
            render={props =>

                !Auth.loggedIn() ? (
                    <Component {...props} />
                ) : (
                        <Redirect
                            to={{
                                pathname: "/timeline",
                                state: { from: props.location }
                            }}
                        />
                    )
            }
        />
    );
}







    export default App;
