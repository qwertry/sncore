import decode from 'jwt-decode';
import {  Cookies } from 'react-cookie';
import axios from 'axios';

const cookies = new Cookies();
export default class AuthService {
    constructor() {
        console.log("authservice ctor called");
        this.login = this.login.bind(this);
        
    }
    login(username, password) {
        let obj = JSON.stringify({
            userName: username,
            password: password
        });
        return axios.post(
            `${AuthService.domain}/Auth/login`,
            obj,
            AuthService.config
        ).then(res => { 
            this.setToken(res.data.token);
            
            
            axios.interceptors.request.use(function (config) {
                console.log(AuthService.config.headers['Authorization']);
                if (this.isTokenExpired(AuthService.config.headers['Authorization'].replace('Bearer ', ''))) {
                    console.log(AuthService.config.headers['Authorization']);
                    console.log('token expired');
                    window.location.href = '/login';
                } else {
                    console.log('token NOT expired');
                }
                return config;
            }.bind(this), function (error) {
                // Do something with request error
                return Promise.reject(error);
                });
            return true;
            }).catch(err => { return err; });
    }

    saveCredentials(username, password) {
        cookies.set('username', username, { path: '/' });
        cookies.set('password', password, { path: '/' });
    }

    loggedIn() {
        const token = this.getToken();
        return !!token && !this.isTokenExpired(token);
    }

    isTokenExpired(token) {
        try {
            const decoded = decode(token);
            if (decoded.exp-10 < Date.now() / 1000) { 
                return true;
            }
            else
                return false;
        }
        catch (err) {
            return false;
        }
    }

    register() {

    }

    setToken(idToken) {
        AuthService.config.headers['Authorization'] = 'Bearer ' + idToken;
       cookies.set('id_token', idToken, { path: '/' });
    }

    getToken() {
        return cookies.get('id_token');
    }

    logout() {
        cookies.remove('id_token');
        AuthService.config.headers['Authorization']='' ;
    }

    getOwnerId() {
        return decode(this.getToken()).sub;
    }

    static config = {
        headers: {
            'accept': 'application/json',
            'Accept-Language': 'en-US,en;q=0.8',
            'Content-Type': `application/json`,
            'Authorization': 'Bearer ' + cookies.get('id_token')

        }
    };

    //static domain = 'http://192.168.1.201:6531/api';
    static domain = 'http://localhost:6531/api';
    

}