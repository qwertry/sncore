
import * as signalR from '@aspnet/signalr';

export default class NotificationService {
  
     connectToHub(userToken) {
        const connection = new signalR.HubConnectionBuilder()
            .withUrl('http://localhost:6531/chathub', { accessTokenFactory: () => userToken })
            .configureLogging(signalR.LogLevel.Information)
            .build();

        connection.on('send', data => {
            console.log(data);
        });
        connection.start()
            .then(() => {
                console.log('Conected');
               // window.location.href = '/';
             });

    }
}