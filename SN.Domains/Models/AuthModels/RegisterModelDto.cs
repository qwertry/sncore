﻿using SN.Domains.Models.Inerfaces;
using System.ComponentModel.DataAnnotations;

namespace SN.Domains.Models.AuthModels
{
    public class RegisterModelDto:ILogable
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string SecondName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [EmailAddress]
        public string Email { get;  set; }
    }
}