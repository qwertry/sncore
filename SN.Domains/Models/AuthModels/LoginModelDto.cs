﻿
using SN.Domains.Models.Inerfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Models.AuthModels
{
    public class LoginModelDto:ILogable
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
