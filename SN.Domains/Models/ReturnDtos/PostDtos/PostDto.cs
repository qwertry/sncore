﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.ReturnDtos.PostDtos
{
    public class PostDto
    {
        public Guid ApplicationUserId { get; set; }
        public int AliasName { get; set; }
        public DateTime PostedAt { get; set; }
        public string Body { get; set; }
    }
}
