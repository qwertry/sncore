﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Models.ReturnDtos.MessageDtos
{
    public class MessageSessionDto
    {
        public Guid MessageSessionId { get; set; }
    }
}
