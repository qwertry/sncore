﻿
using SN.Domains.Entities;
using SN.Domains.Enums;
using SN.Domains.Models.ReturnDtos.UserDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Models.ReturnDtos.RequestDtos
{
    public class FriendRequestDto
    {
        public Guid SenderId { get; set; }
        public UserDto Sender { get; set; }
        public DateTime SendingDate { get; set; }
    }
}
