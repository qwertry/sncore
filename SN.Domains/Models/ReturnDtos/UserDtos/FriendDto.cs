﻿using SN.Domains.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Models.ReturnDtos.UserDtos
{
    public class FriendDto
    {
        public UserDto UserFriend { get; set; }
    }
}
