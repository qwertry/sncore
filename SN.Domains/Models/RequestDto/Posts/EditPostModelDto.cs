﻿using SN.Domains.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Models.RequestDto.Posts
{
    public class EditPostModelDto
    {
        public int AliasName { get; set; }
        [MaxLength(500)]
        [MinLength(1)]
        public string Body { get; set; }
        public List<Guid> PermitedUsers { get; set; }
        public PostPermissionTypes PostPermissionType { get; set; }
    }
}
