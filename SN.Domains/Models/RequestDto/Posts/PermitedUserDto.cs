﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Models.RequestDto.Posts
{
    public class PermitedUserDto
    {
        public Guid UserId { get; set; }
    }
}
