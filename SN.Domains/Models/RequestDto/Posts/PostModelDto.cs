﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using SN.Domains.Entities;
using SN.Domains.Enums;
using SN.Domains.Models.RequestDto.Posts;

namespace SN.Domains.Models.RequestDto.Posts
{
    public class PostModelDto
    {
        public int AliasName { get; set; }
        [MaxLength(500)]
        public string Body { get; set; }
        public List<PermitedUserDto> PermitedUsers { get; set; }
        public PostPermissionTypes PostPermissionType { get; set; }
    }
}
