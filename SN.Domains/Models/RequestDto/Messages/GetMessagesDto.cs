﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Models.RequestDto.Messages
{
    public class GetMessagesDto
    {
        [Required]
        public int From { get; set; }
        [Required]
        public int To { get; set; }
        [Required]
        public Guid MessageSessionId { get; set; }
    }
}
