﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Models.RequestDto.Messages
{
    public class MessageToUserDto
    {
        [Required]
        public Guid ResieverId { get; set; }
        [Required]
        public string MessageBody { get; set; }
    }
}
