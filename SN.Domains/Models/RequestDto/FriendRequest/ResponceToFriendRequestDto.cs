﻿using SN.Domains.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace SN.Domains.FriendRequest
{
    public class ResponceToFriendRequestDto
    {
        [Required]
        public Guid FromUser { get; set; }
        [Required]
        public FriendRequestStatus Status { get; set; }
    }   
}