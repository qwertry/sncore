﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SN.Domains.Models.RequestDto.FriendRequest
{
    public class FriendRequestDto
    {
        [Required]
        public Guid ToUser { get; set; }
    }
}