﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SN.Domains.Enums
{
    public enum StatusCode
    {
        Ok = 200,
        BadRequest = 400,
        Unauthorized = 401,
        ServerError = 500,
        NotFound = 404
    }
}
