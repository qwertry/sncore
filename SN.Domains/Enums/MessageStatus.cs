﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Enums
{
    public enum MessageStatus
    {
        Sent,
        Delivered,
        Seen
    }
}
