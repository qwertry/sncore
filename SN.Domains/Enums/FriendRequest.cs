﻿namespace SN.Domains.Enums
{
    public enum FriendRequestStatus
    {
        Pending, Denied, Accepted
    }
} 