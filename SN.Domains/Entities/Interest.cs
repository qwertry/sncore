﻿using System;
using System.Collections.Generic;

namespace SN.Domains.Entities
{
    public class Interest
    {
        public Guid InterestId { get; set; }
        public string InterestName { get; set; }
        public List<UserInterest> UserInterests { get; set; }
    }
}