﻿using System;

namespace SN.Domains.Entities
{
    public class Image
    {
        public Guid ImageId { get; set; }

        public byte[] ImageBody { get; set; }
    }
}