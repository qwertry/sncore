﻿using SN.Domains.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Entities
{
    public class Message
    {
        public Guid MessageId { get; set; }
        public Guid MessageSessionId { get; set; }
        public MessageSession MessageSession { get; set; }
        public string MessageBody { get; set; }
        public Guid SenderId { get; set; }
        public Guid RecieverId { get; set; }
        public DateTime SendingDate { get; set; }
        public MessageStatus Status { get; set; }
    }
}
