﻿using SN.Domains.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Entities
{
    public class Post
    {
        public Guid PostId { get; set; }
        public Guid ApplicationUserId { get; set; }
        public int AliasName { get; set; }
       // [ForeignKey("ApplicationUserId")]
        public ApplicationUser ApplicationUser { get; set; }
        public DateTime PostedAt { get; set; }
        public string Body { get; set; }
        public List<PostComment> Comments { get; set; }
        public List<PermitedUser> PermitedUsers { get; set; }
        public PostPermissionTypes PostPermissionType { get; set; }
        public List<Image> Images { get; set; }
    }
}
