﻿using SN.Domains.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Entities
{
    public class FriendRequest
    {
        public Guid FriendRequestId { get; set; }
        public Guid SenderId { get; set; }
       // [ForeignKey("SenderId")]
        public ApplicationUser Sender { get; set; }
        public Guid RecipientId { get; set; }
        public DateTime SendingDate {get;set;}
    }
}
