﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace SN.Domains.Entities
{
   

    public class ApplicationUser : IdentityUser<Guid>
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string About { get; set; }
        public List<UserInterest> UserInterests { get; set; }
        public List<Post> Posts { get; set; }
        public List<FriendRequest> FriendRequests { get; set; }
        public List<Friend> Friends { get; set; }
        public List<MessageSession> MessageSessions { get; set; }
        public List<Image> Images { get; set; }
        public byte[] Avatar { get; set; }

    }
}
