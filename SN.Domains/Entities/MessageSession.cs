﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Entities
{
    public class MessageSession
    {
        public Guid MessageSessionId { get; set; }
        public List<Message> Messages { get; set; }
        public Guid ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        public Guid RecieverId { get; set; }
    }
}
