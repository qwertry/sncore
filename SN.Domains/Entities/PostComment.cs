﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Entities
{
    public class PostComment
    {
        public Guid PostCommentId { get; set; }
        public Post Post { get; set; }
        public Guid PostId { get; set; }
        public Guid ApplicationUserId { get; set; }
        public DateTime PostedAt { get; set; }
        public string Body { get; set; }
    }
}
