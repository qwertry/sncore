﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SN.Domains.Entities
{
    public class UserInterest
    {
        public Guid UserInterestId { get; set; }


        public Guid AppicationUserId { get; set; }
        public ApplicationUser AppicationUser { get;  set; }

        public Guid InterestId { get; set; }
        public Interest Interest { get; set; }
    }
}