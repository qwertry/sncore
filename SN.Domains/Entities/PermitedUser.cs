﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.Domains.Entities
{
    public class PermitedUser
    {
        public Guid PermitedUserId { get; set; }
        public Post Post { get; set; }
        public Guid PostId { get; set; }
        public Guid ApplicationUserId { get; set; }
    }
}
