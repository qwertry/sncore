﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SN.Domains.Entities
{
    public class Friend
    {
        
        public Guid FriendId { get; set; }
        public Guid UserId { get; set; }
        public Guid UserFriendId { get; set; }
       // [ForeignKey("UserFriendId")]
        public ApplicationUser UserFriend { get; set; }
        public DateTime FriendshipStrartTime { get; set; }
    }
}