﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SN.Domains.Entities;
using SN.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace SN.Tests
{
	public class BaseTest
	{
		public ApplicationDbContext _dbContext { get; private set; }
		public BaseTest()
		{
			var serviceCollection = new ServiceCollection();
			serviceCollection.AddDbContext<ApplicationDbContext>(
			  o => o.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=SN_DB;Trusted_Connection=True;MultipleActiveResultSets=true")
		   );
			var serviceProvider = serviceCollection.BuildServiceProvider();
			_dbContext = serviceProvider.GetService<ApplicationDbContext>();
		}
	}
}
