﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SN.Domains.Entities;
using SN.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace SN.Tests.Infrastructure
{
	public class PostRepositoryTests:BaseTest
	{
		[Fact]
		public async void GetFriendsPosts()
		{
			var posts = await new PostRepository(_dbContext).GetFriendsPosts(Guid.Parse("3555a8d3-6fd5-4941-9125-08d675905aaa"));
			Assert.True(posts.Count()==6);
		}
	}
}
