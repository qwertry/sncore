This is a small social network project, where back-end technologies include: ASP .NET Core 2.0,  EF Core,  Web Sockets (SignalR),  Core Identity, fully designed by Harutyun Gazyan.
The main goal in back end is reaching clean architecture.  
Front end apps are React (Harutyun Gazyan's educational project) & Angular 6 (Hamlet Ayvazyan's educational project).