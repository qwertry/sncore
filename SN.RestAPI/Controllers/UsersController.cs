﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SN.BuisnessLogic.Repositories;
using SN.Domains.Entities;
using  SN.Domains.Models.ReturnDtos.UserDtos;
using SN.Domains.ReturnDtos.PostDtos;

namespace SN.RestAPI.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class UsersController : Controller
    {
        private IUserRepository _userRepository;
        private ApplicationUser currentUser;
        private readonly UserManager<ApplicationUser> _userManager;
        public UsersController(IUserRepository userRepository, UserManager<ApplicationUser> userManager)
        {
            _userRepository = userRepository;
            _userManager = userManager;

        }

        public override void OnActionExecuting(ActionExecutingContext ctx)
        {
            base.OnActionExecuting(ctx);
            currentUser = _userManager.GetUserAsync(HttpContext.User).Result;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {

            var data = await _userRepository.GetAllAsync();

            return Json(data.ProjectTo<UserDto>().ToList());
        }
        [HttpGet]
        public async Task<IActionResult> GetMyData()
        {
            Guid ownerId = currentUser.Id;

            var user = await _userRepository.GetByIdAsync(ownerId);
            if (user == null)
            {
                return Unauthorized();
            }
            var json = Mapper.Map<UserDto>(user);
            return Json(json);
        }

        [AllowAnonymous]
        [HttpGet("id")]
        public IActionResult AdoTest(Guid id)
        {
            var adoData = new List<PostDto>();
            using (var connection = new SqlConnection("Server=(localdb)\\mssqllocaldb;Database=SN_DB;Trusted_Connection=True;MultipleActiveResultSets=true"))
            {
                connection.Open();
                SqlCommand command = new SqlCommand();

                SqlParameter param = new SqlParameter("Id", id);

                SqlCommand cmd = new SqlCommand("GetPostsAvailableForUser", connection);
                cmd.Parameters.Add(param);

                cmd.CommandType = CommandType.StoredProcedure;


                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows) // если есть данные
                {

                    while (reader.Read()) // построчно считываем данные
                    {
                        var adoPost = new PostDto();
                        adoPost.Body = reader.GetString(reader.GetOrdinal("Body"));
                        adoPost.PostedAt = reader.GetDateTime(reader.GetOrdinal("PostedAt"));
                        adoPost.AliasName = reader.GetInt32(reader.GetOrdinal("AliasName"));
                        adoPost.ApplicationUserId = reader.GetGuid(reader.GetOrdinal("ApplicationUserId"));
                        adoData.Add(adoPost);
                    }
                }

                reader.Close();
            }
            return Json(adoData);
        }


    }
}