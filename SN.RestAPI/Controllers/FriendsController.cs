﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

using SN.RestAPI.Attributes;
using SN.Domains.Entities;
using SN.Domains.FriendRequest;
using SN.BuisnessLogic.FriendsLogic;
using SN.Domains.Models.RequestDto.FriendRequest;

namespace SN.RestAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    [Authorize]
    public class FriendsController : Controller
    {
        private readonly FriendsLogic _friendsLogic;
        public FriendsController(UserManager<ApplicationUser> userManager, 
            FriendsLogic friendsLogic,
            IHttpContextAccessor httpContextAccessor)
        {
            _friendsLogic = friendsLogic;

            var context = httpContextAccessor.HttpContext;

            _friendsLogic.CurrentUser = userManager.GetUserAsync(context.User).Result;

        }
        
        [HttpPost]
        [ModelValidation]
        public async Task<IActionResult> FriendRequest([FromBody] FriendRequestDto request)
        {
            var result = await _friendsLogic.PlaceFriendRequestAsync(request);
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }

        [HttpPost]
        [ModelValidation]
        public async Task<IActionResult> DeleteFriendRequest([FromBody] FriendRequestDto request)
        {
            var result = await _friendsLogic.DeleteFriendRequestAsync(request);
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }

        [HttpPost]
        [ModelValidation]
        public async Task<IActionResult> DeleteFriend([FromBody] FriendRequestDto request)
        {

            var result = await _friendsLogic.DeleteFriendAsync(request);
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }

        [HttpPost]
        [ModelValidation]
        public async Task<IActionResult> ResponceToFriendRequest([FromBody] ResponceToFriendRequestDto request)
        {

            var result = await _friendsLogic.ResponceToFriendRequestAsync(request);
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }

        
        [HttpGet]
        public async Task<IActionResult> GetAllFriendRequests()
        {
            var result = await _friendsLogic.GetAllFriendRequestsAsync();
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }

        [HttpGet]
        public async Task<IActionResult> GetAllFriends()
        {

            var result = await _friendsLogic.GetAllFriendsAsync();
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }
        
    }
}