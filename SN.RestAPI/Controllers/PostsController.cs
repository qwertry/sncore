﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SN.Domains.Entities;
using SN.RestAPI.Attributes;
using SN.Domains.Models.RequestDto.Posts;
using SN.BuisnessLogic.BuisnesLogic;

namespace SN.RestAPI.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]

    public class PostsController : Controller
    {
        private ApplicationUser currentUser;
        private PostsLogic _postsLogic;
        public PostsController(UserManager<ApplicationUser> userManager,
            IHttpContextAccessor httpContextAccessor,
            PostsLogic postsLogic)
        {
            _postsLogic = postsLogic;

            var context = httpContextAccessor.HttpContext;

            _postsLogic.CurrentUser = userManager.GetUserAsync(context.User).Result;
        }

        

        [HttpPost]
        [ModelValidation]
        public async Task<IActionResult> Create([FromBody] PostModelDto post )
        {
            var result = await _postsLogic.CreateAsync(post);
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }

        [HttpPost]
        [ModelValidation]
        public async Task<IActionResult> Edit([FromBody] PostModelDto post)
        {
            var result = await _postsLogic.EditAsync(post);
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }

        [HttpPost]
        [ModelValidation]
        public async Task<IActionResult> Delete([FromBody] DeletePostModelDto post)
        {
            var result = await _postsLogic.DeleteAsync(post);
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }

        
        [HttpGet]
        public async Task<IActionResult> FriendsPosts()
        {
            var result = await _postsLogic.GetFriendsPostsAsync();
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }
    }
}