﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SN.BuisnessLogic.AuthLogic;
using SN.Domains.Entities;
using SN.Domains.Models.AuthModels;
using SN.RestAPI.Attributes;

namespace SN.RestAPI.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class AuthController : Controller
    {
        private readonly AuthLogic _authLogic;

        public AuthController(AuthLogic authLogic)
        {
            _authLogic = authLogic;
        }

        [HttpPost]
        [ModelValidation]
        public async Task<IActionResult> Login([FromBody] LoginModelDto model)
        {
            var result = await _authLogic.LoginAsync(model);
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }

        [HttpPost]
        [ModelValidation]
        public async Task<IActionResult> Register([FromBody] RegisterModelDto model)
        {
            var result = await _authLogic.RegisterAsync(model);
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }

    }
}