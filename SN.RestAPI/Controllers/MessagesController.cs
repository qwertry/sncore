﻿
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SN.Domains.Entities;
using  SN.Domains.Models.RequestDto.Messages;
using SN.RestAPI.Attributes;
using SN.BuisnessLogic.BuisnesLogic;

namespace SN.RestAPI.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]/[action]")]
    public class MessagesController : Controller
    {
        private MessagesLogic _messagesLogic;
        public MessagesController(
            UserManager<ApplicationUser> userManager,
            IHttpContextAccessor httpContextAccessor,
            MessagesLogic messagesLogic)
        {
            _messagesLogic = messagesLogic;

            var context = httpContextAccessor.HttpContext;

            _messagesLogic.CurrentUser = userManager.GetUserAsync(context.User).Result;
        }

        

        [HttpPost]
        [ModelValidation]
        public async Task<IActionResult> SendMessage([FromBody] MessageToUserDto message)
        {
            var result = await _messagesLogic.SendMessageAsync(message);
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }

        [HttpGet]
        public async Task<IActionResult> GetMessageSessions()
        {
            var result = await _messagesLogic.GetMessageSessionsAsync();
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }

        [HttpPost]
        [ModelValidation]
        public async Task<IActionResult> GetMessages([FromBody] GetMessagesDto messageModel)
        {
            var result = await _messagesLogic.GetMessagesAsync(messageModel);
            return new JsonResult(result.Data)
            {
                StatusCode = (int)result.StatusCode // Status code here 
            };
        }

       
    }
}