﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using SN.Domains.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using  SN.Domains.Models.ReturnDtos.UserDtos;
using  SN.Domains.Models.ReturnDtos.RequestDtos;
using  SN.Domains.Models.ReturnDtos.MessageDtos;
using Microsoft.AspNetCore.SignalR;
using SN.RestAPI.Notifications;
using SN.BuisnessLogic.AuthLogic;
using SN.BuisnessLogic.FriendsLogic;
using SN.BuisnessLogic.BuisnesLogic;
using SN.Domains.ReturnDtos.PostDtos;
using SN.BuisnessLogic.Repositories;
using SN.Infrastructure;

namespace SN
{
    public class Startup
    {
        public static IConfigurationRoot Configuration { get; private set; }
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true)
            .AddJsonFile($"appSettings.{env.EnvironmentName}.json", optional: false, reloadOnChange: true)
            ;
            Configuration = builder.Build();
        }
        public Startup(IConfigurationRoot configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(
               o => o.UseSqlServer(
                Configuration.GetConnectionString("DefaultConnection")
                )
            );

            //services.AddIdentity<ApplicationUser, UserRole>(options =>
            //{
            //    options.User.RequireUniqueEmail = true;

            //}).AddUserStore<ApplicationUserStore>()
            //        .AddRoleStore<ApplicationRoleStore>()
            //        .AddEntityFrameworkStores<ApplicationDbContext>()
            //        .AddDefaultTokenProviders();

            services.AddIdentity<ApplicationUser, IdentityRole<Guid>>()
                 .AddEntityFrameworkStores<ApplicationDbContext>()
                 .AddDefaultTokenProviders();

            services.AddScoped<IPostRepository, PostRepository>();
            services.AddScoped<IFriendRepository, FriendRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IMessageSessionRepository, MessageSessionRepository>();
            services.AddScoped<IMessageRepository, MessageRepository>();
            services.AddScoped<IJWTRepository, JWTRepository>();
            services.AddScoped<IHubRepository, HubRepository>();

            services.AddScoped<AuthLogic>();
            services.AddScoped<FriendsLogic>();
            services.AddScoped<PostsLogic>();
            services.AddScoped<MessagesLogic>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                //options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(o =>
           {
               o.SaveToken = true;
               o.RequireHttpsMetadata = false;
               o.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
               {
                   ValidateIssuer = true,
                   ValidateAudience = true,
                   ValidAudience = "http://oec.com",
                   ValidIssuer = "http://oec.com",
                   ValidateLifetime = true,
                   IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("lalalallalallalalallalallalalallalal"))
               };
               o.Events = new JwtBearerEvents
               {
                   OnMessageReceived = context =>
                   {
                       var accessToken = context.Request.Query["access_token"];

                        // If the request is for our hub...
                        var path = context.HttpContext.Request.Path;
                       if (!string.IsNullOrEmpty(accessToken) &&
                           (path.StartsWithSegments("/chathub")))
                       {
                           // Read the token out of the query string
                           
                           context.Token = accessToken;
                           
                       }
                       return Task.CompletedTask;
                   }
               };
           });
            services.AddCors(options => options.AddPolicy("CorsPolicy",
           builder =>
           {
               builder
           .AllowAnyOrigin()
           .AllowAnyMethod()
           .AllowAnyHeader()
           .AllowCredentials();
           }
           ));
            services.AddSignalR();
            services.AddSingleton<IUserIdProvider, NameUserIdProvider>();
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory
            )
        {
            loggerFactory.AddConsole();
            loggerFactory.AddDebug();
            loggerFactory.AddProvider(new NLog.Extensions.Logging.NLogLoggerProvider());
            //loggerFactory.AddNlog();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler();
            }

            app.UseStatusCodePages();

            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ApplicationUser, UserDto>();
                cfg.CreateMap<FriendRequest, FriendRequestDto>();
                cfg.CreateMap<Post, PostDto>();
                cfg.CreateMap<Friend, FriendDto>();
                cfg.CreateMap<MessageSession, MessageSessionDto>();
                cfg.CreateMap<Message, MessageDto>();
            });

            app.UseAuthentication();

            app.UseCors("CorsPolicy");

            app.UseSignalR(routes =>
            {
                routes.MapHub<Chat>("/chathub");
            });

            app.UseMvc();

            SeedDatabase.Initialize(app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope().ServiceProvider);


        }
    }

}
