import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {Posts} from './post';
import {MessageService} from '../message.service';
import {environment} from '../../environments/environment';

const API_URL = environment.apiUrl;
const httpOptions = {
    headers: new HttpHeaders({
        'Authorization': 'bearer '
    })
};

@Injectable({
    providedIn: 'root'
})
export class PostService {

    private postUrl = API_URL + '/api/posts/create';

    constructor(private http: HttpClient, private messageService: MessageService) {
    }

    /** POST: add a new post to the database */
    addPost(post: Posts): Observable<Posts> {
        return this.http.post<Posts>(this.postUrl, post, httpOptions)
            .pipe(
                catchError(this.handleError('addPost', post))
            );
    }


    /**
     * Handle Http operation that failed.
     * Let the app continue.
     * @param operation - name of the operation that failed
     * @param result - optional value to return as the observable result
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {

            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead

            // TODO: better job of transforming error for user consumption
            this.log(`${operation} failed: ${error.message}`);

            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    /** Log a PostService message with the MessageService */
    private log(message: string) {
        this.messageService.add(`HeroService: ${message}`);
    }
}
