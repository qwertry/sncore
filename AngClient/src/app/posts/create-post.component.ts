import {Component, OnInit} from '@angular/core';
import {PostService} from './post.service';

@Component({
    selector: 'app-create-post',
    templateUrl: './create-post.component.html',
    styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {

    constructor(private postService: PostService) {
    }

    ngOnInit() {
    }

    createPost(value) {
        if (value.body === undefined) {
            return;
        } else {
            this.postService.addPost(value).subscribe(post => {
                console.log(post);
            }, err => console.log(err));
        }
        console.log(value);
    }

}
