import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';
import {RegisterComponent} from './auth/register/register.component';
import {LoginComponent} from './auth/login/login.component';
import { HomeComponent } from './home/home.component';
import { LayoutComponent } from './layout/layout.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { PostsComponent } from './posts/posts.component';
import { UserComponent } from './user/user.component';
import { AppRoutingModule } from './/app-routing.module';
import {httpInterceptorProviders} from './http-interceptors';
import { SidebarComponent } from './home/sidebar.component';
import { RequestsComponent } from './user/requests.component';
import { UserViewComponent } from './user/user-view/user-view.component';
import { HomeLayoutComponent } from './home/home-layout.component';
import { MessagesComponent } from './messages/messages.component';
import { CreatePostComponent } from './posts/create-post.component';



@NgModule({
    declarations: [
        AppComponent,
        RegisterComponent,
        LoginComponent,
        HomeComponent,
        LayoutComponent,
        PageNotFoundComponent,
        PostsComponent,
        UserComponent,
        SidebarComponent,
        RequestsComponent,
        UserViewComponent,
        HomeLayoutComponent,
        MessagesComponent,
        CreatePostComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AngularFontAwesomeModule,
        BrowserAnimationsModule,
        ReactiveFormsModule, FormsModule,
        AppRoutingModule,
    ],
    providers: [
        httpInterceptorProviders
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
