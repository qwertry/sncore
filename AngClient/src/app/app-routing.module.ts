import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HomeComponent} from './home/home.component';
import {LoginGuard} from './auth/login/login.guard';
import {UserComponent} from './user/user.component';
import {RegisterComponent} from './auth/register/register.component';
import {LoginComponent} from './auth/login/login.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {PostsComponent} from './posts/posts.component';
import {UserViewComponent} from './user/user-view/user-view.component';
import {MessagesComponent} from './messages/messages.component';
import {CreatePostComponent} from './posts/create-post.component';

const routes: Routes = [
    {
        path: 'home', component: HomeComponent, canActivate: [LoginGuard], children: [
            {
                path: '',
                canActivateChild: [LoginGuard],
                children: [
                    {path: '', redirectTo: 'user', pathMatch: 'full'},
                    {path: 'user', component: UserComponent},
                    {path: 'posts', component: PostsComponent},
                    {path: 'createPost', component: CreatePostComponent},
                    {path: 'view/:id', component: UserViewComponent},
                ]
            }
        ]
    },
    {path: 'auth', component: RegisterComponent},
    {path: 'login', component: LoginComponent},
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: '**', component: PageNotFoundComponent, pathMatch: 'full'},
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            routes,
            // {enableTracing: true} // <-- debugging purposes only
        )
    ],
    exports: [
        RouterModule
    ],
    declarations: []
})
export class AppRoutingModule {
}
