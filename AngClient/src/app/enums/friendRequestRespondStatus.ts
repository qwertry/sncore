export enum FriendRespondStatus {
    Denie = 1,
    Approve = 2
}
