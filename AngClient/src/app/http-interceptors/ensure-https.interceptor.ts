import {Injectable} from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

@Injectable()
export class EnsureHttpsInterceptor implements HttpInterceptor {


    intercept(req: HttpRequest<any>, next: HttpHandler) {
        // clone request and replace 'http://' with 'https://' at the same time
        const secureReq = req.clone({
            url: req.url.replace('http://', 'http://')
        });
// send the cloned, "secure" request to the next handler.
        return next.handle(secureReq);
    }
}
