import {Injectable} from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';
import {MessageService} from '../message.service';
import {finalize, tap} from 'rxjs/operators';

@Injectable()
export class LoggingInterceptor implements HttpInterceptor {
    constructor(private messenger: MessageService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const started = Date.now();
        let ok: string;

        // extend server response observable with logging
        return next.handle(req)
            .pipe(
                tap(
                    // Succeeds when there is a response; ignore other events
                    event => ok = event instanceof HttpResponse ? 'succeeded' : 'failed to load',
                    // Operation failed; error is an HttpErrorResponse
                    error => {
                        ok = 'failed: ';
                        this.checkErr(error);
                    }
                ),
                // Log when response observable either completes or errors
                finalize(() => {
                    const elapsed = Date.now() - started;
                    const msg = `${req.method} "${req.urlWithParams}"
             ${ok} in ${elapsed} ms.`;
                    this.messenger.add(msg);
                })
            );
    }

    checkErr(err) {
        if (err.status === 401) {
            console.log('error 401 found');
        } else {
            console.log(err);
        }
    }
}
