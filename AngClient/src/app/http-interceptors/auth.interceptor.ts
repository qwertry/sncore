import {Injectable} from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {AuthService} from '../auth/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private auth: AuthService) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        if (this.auth.isLoggedIn) {
            console.log('login checked ok');
            const currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
            if (currentUser && currentUser.token) {
                const authReq = req.clone({
                    headers: req.headers.set('Authorization', `bearer ${currentUser.token}`)
                });
                return next.handle(authReq);
            }
        } else {
            console.log('is not logged in');
            const authToken = '';
            const authReq = req.clone({
                headers: req.headers.set('Authorization', `${authToken}'`)
            });
            return next.handle(authReq);
        }
    }
}
