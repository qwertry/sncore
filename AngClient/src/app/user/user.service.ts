import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {FriendRespondStatus} from '../enums/friendRequestRespondStatus';
import {catchError, map, tap} from 'rxjs/operators';

import {Users} from '../users';
import {throwError, Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import { IFriendRequest } from '../datamodels/ResponceDtos/IFriendRequest';

const API_URL = environment.apiUrl;
const httpOptions = {
    headers: new HttpHeaders({'Authorization': 'bearer '})
};

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private getUsersUrl = API_URL + '/api/Users/GetUsers';
    private GetMyDataUrl = API_URL + '/api/Users/GetMyData';
    private getFriendRequestsUrl = API_URL + '/api/friends/GetAllFriendRequests';
    private respondToFriendRequest = API_URL + '/api/friends/ResponceToFriendRequest';
    private friendRequestUrl = API_URL + '/api/friends/friendrequest';

    constructor(private http: HttpClient) {
    }

    getUsers() {
        return this.http.get(this.getUsersUrl, httpOptions);
    }

    getUser(id: number | string) {
        return this.getFriendRequests().pipe(
            map((users: Users[]) => users.find(user => user.senderId === id))
        );
    }

    friendRequest(id: string) {
        const body = {
            toUser: id
        };
        return this.http.post(this.friendRequestUrl, body, httpOptions);
    }

  getFriendRequests(): Observable<IFriendRequest[]> {
    return this.http.get<IFriendRequest[]>(this.getFriendRequestsUrl, httpOptions)
            .pipe(
                tap(), catchError(this.handleError)
            );
    }


    respondToFriendRequests(respondableUserId: string, respondeStatus: FriendRespondStatus) { 
        const body = {
            fromUser: respondableUserId,
            status: respondeStatus
        };
        return this.http.post(this.respondToFriendRequest, body, httpOptions);
    }

    getMyData() {
        return this.http.get(this.GetMyDataUrl, httpOptions)
            .pipe(
                tap(), catchError(this.handleError)
            );
    }


    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }
}
