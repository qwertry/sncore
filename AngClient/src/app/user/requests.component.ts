import {switchMap} from 'rxjs/operators';
import {Component, OnInit} from '@angular/core';

import {Observable} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

import {Users} from '../users';
import {UserService} from './user.service';
import {FriendRespondStatus} from '../enums/friendRequestRespondStatus';

import {HubConnection, HubConnectionBuilder} from '@aspnet/signalr';
import * as signalR from '@aspnet/signalr';
import {AuthService} from '../auth/auth.service';
import {IFriendRequest} from '../datamodels/ResponceDtos/IFriendRequest';

@Component({
    selector: 'app-requests',
    templateUrl: './requests.component.html',
    styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {
    public _hubConnection: HubConnection;
    friendRequests: IFriendRequest[];
    users$: Observable<Users[]>;
    selectedId;

    constructor(private userService: UserService,
                private authService: AuthService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        // @ts-ignore
        this.users$ = this.route.paramMap.pipe(
            switchMap(params => {
                // (+) before `params.get()` turns the string into a number
                this.selectedId = params.get('id');
                return this.userService.getFriendRequests();
            })
        );

        this.userService.getFriendRequests().subscribe(f => {
            this.friendRequests = f;
            console.log('I am here');
            console.log(this.friendRequests);
        }, err => {
            console.log(err);
        });
        this.authService.connectToHub();
    }

    responceToFriendRequest(o, status: FriendRespondStatus) {
        this.userService.respondToFriendRequests(o.senderId, status).subscribe(f => {
            console.log(f);
            this.userService.getFriendRequests().subscribe(f => {
                this.friendRequests = f;
                console.log(this.friendRequests);
            }, err => {
                console.log(err);
            });
        }, err => {
            console.log(err);
        });
        console.log(o.senderId);
        console.log(status);
    }
}
