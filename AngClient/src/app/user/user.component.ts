import {Component, OnInit} from '@angular/core';
import {UserService} from './user.service';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

    allUsers = [];

    constructor(private userService: UserService) {
    }

    ngOnInit() {
        this.userService.getFriendRequests().subscribe(f => {
            console.log(f);
        }, err => {
            console.log(err);
        });
        this.getUsers();
    }

    getUsers() {
        this.userService.getUsers().subscribe(users => {
            this.allUsers.push(users);
            console.log(this.allUsers);
        }, err => {
            console.log(err);
        });
    }

    sendRequest(id) {
        this.userService.friendRequest(id).subscribe(data => {
            console.log(data);
        }, err => console.log(err));
    }
}
