import { switchMap } from 'rxjs/operators';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';

import {Users} from '../../users';
import {UserService} from '../user.service';

@Component({
    selector: 'app-user-view',
    templateUrl: './user-view.component.html',
    styleUrls: ['./user-view.component.css']
})
export class UserViewComponent implements OnInit {
    user$: Observable<Users[]>;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private userService: UserService
    ) {}

    ngOnInit() {
        // @ts-ignore
        this.user$ = this.route.paramMap.pipe(
            switchMap((params: ParamMap) =>
                this.userService.getUser(params.get('id')))
        );
    }
}
