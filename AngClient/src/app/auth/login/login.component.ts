import {Component, OnInit} from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import {AuthService} from '../auth.service';

@Component({
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    returnUrl: string;

    constructor(private authService: AuthService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        this.authService.logout();
        // get return url from route parameters or default to '/'
       // this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
        this.returnUrl = this.route.snapshot.children['url'];
    }

    initLogin(value) {
        if (value.userName === undefined && value.password === undefined) {
            return;
        } else {
            console.log(value);
            this.authService.login(value).subscribe(
                 data => {
                     this.router.navigateByUrl(this.returnUrl);
                     window.location.reload();
                 },
                 err => console.log('error: ', err)
            );
        }
    }

    reloadPage() {
        window.location.reload();
    }
}
