import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanActivateChild} from '@angular/router';
import {AuthService} from '../auth.service';

@Injectable({
    providedIn: 'root'
})
export class LoginGuard implements CanActivate, CanActivateChild {

    constructor(private authService: AuthService, private router: Router) {
    }

    static checkTokenValidity(): boolean {
        if (sessionStorage.getItem('currentUser')) {
            const currentToken = JSON.parse(sessionStorage.getItem('currentUser'));
            const d = new Date().toISOString();
            console.log('is logged in with current token exp: ' + currentToken.expiration, 'and current time is: ' + d);
            return currentToken.expiration > d;
        }
        return false;
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): boolean {
        const url: string = state.url;

        return this.checkLogin(url);
    }

    canActivateChild(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): boolean {
        return this.canActivate(route, state);
    }

    checkLogin(url: string): boolean {
        if (LoginGuard.checkTokenValidity()) {
            this.authService.isLoggedIn = true;
            return true;
        }

        // Store the attempted URL for redirecting
        this.authService.redirectUrl = url;

        // Navigate to the login page with extras
        this.router.navigate(['/login']);
        this.authService.isLoggedIn = false;
        return false;
    }
}
