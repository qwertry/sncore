import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../auth.service';

@Component({
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    returnUrl: string;

    constructor(private authService: AuthService,
                private router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.authService.logout();
        this.returnUrl = this.route.snapshot.children['url'];
    }

    initRegister(value) {
        if (value.userName === undefined && value.password === undefined && value.email === undefined) {
            return;
        } else {
            this.authService.register(value).subscribe(
                data => {
                        this.router.navigateByUrl(this.returnUrl);
                        window.location.reload();
                },
                err => console.log('error: - ', err)
            );
        }
    }

    reloadPage() {
        window.location.reload();
    }
}
