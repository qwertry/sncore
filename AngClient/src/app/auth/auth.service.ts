import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';

import {Observable, of, throwError} from 'rxjs';
import {tap, delay, catchError, map} from 'rxjs/operators';

import {ILogin} from './login/login';
import {MessageService} from '../message.service';
import {IRegister} from './register/register';
import {environment} from '../../environments/environment';

import {HubConnection, HubConnectionBuilder} from '@aspnet/signalr';
import * as signalR from '@aspnet/signalr';

const API_URL = environment.apiUrl;
const httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    isLoggedIn = false;
    private loginUrl = API_URL + '/api/auth/login';
    private registerUrl = API_URL + '/api/auth/register';
    // store the URL so we can redirect after logging in
    redirectUrl: string;
    token: string;

    constructor(
        private http: HttpClient,
        private guidService: MessageService
    ) {
    }

    public connectToHub() {
        let jsonUser = JSON.parse(sessionStorage.getItem('currentUser'));
        // let userToken = "bearer " + jsonUser.token;
        let userToken = jsonUser.token;
        console.log(userToken);

        const connection = new signalR.HubConnectionBuilder()
            .withUrl('http://localhost:6531/chathub', {accessTokenFactory: () => userToken})
            .configureLogging(signalR.LogLevel.Information)
            .build();

        connection.on('send', data => {
            console.log(data);
        });
        connection.start()
            .then(() => console.log('Conected'));
    }

    login(value): Observable<ILogin> {
        return this.http.post<ILogin>(this.loginUrl, value, httpOptions)
            .pipe(
                tap(user => {
                    // @ts-ignore
                    if (user && user.token) {
                        sessionStorage.setItem('currentUser', JSON.stringify(user));
                        // @ts-ignore
                        this.token = user.token;
                    }
                    return user;
                }, err => {
                    this.isLoggedIn = false;
                    this.log(err);
                }),
                catchError(this.handleError)
            );
    }

    register(auth): Observable<IRegister> {
        return this.http.post<IRegister>(this.registerUrl, auth, httpOptions)
            .pipe(
                tap(user => {
                    // @ts-ignore
                    if (user && user.token) {
                        sessionStorage.setItem('currentUser', JSON.stringify(user));
                        // @ts-ignore
                        this.token = user.token;
                    }
                    this.log('register success');
                    return user;

                }, err => {
                    this.isLoggedIn = false;
                    this.log(err);
                }),
                catchError(this.handleError)
            );
    }

    logout(): void {
        sessionStorage.removeItem('currentUser');
        this.isLoggedIn = false;
        this.token = null;
    }


    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }

    /** Log a MessageService message with the MessageService */
    private log(message: string) {
        this.guidService.add(`GuidService: ${message}`);
    }
}
