import {Component, OnInit} from '@angular/core';
import {MessageService} from '../message.service';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

    title = 'Friends';

    constructor(public messageService: MessageService) {
    }

    ngOnInit() {
    }

}
