
export class PostDto {
    postedAt;
    body;
    aliasName;
    authorName;

    constructor(post, friend) {
        this.postedAt = post.postedAt;
        this.body = post.body;
        this.aliasName = post.aliasName;
        this.authorName = friend.firstName + ' ' + friend.secondName;
    }
}
