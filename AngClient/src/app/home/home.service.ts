import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {throwError} from 'rxjs';

import {MessageService} from '../message.service';
import {environment} from '../../environments/environment';


const API_URL = environment.apiUrl;
const httpOptions = {
    headers: new HttpHeaders({'Authorization': 'bearer '})
};

@Injectable({
    providedIn: 'root'
})
export class HomeService {
    private getFriendsPostsUrl = API_URL + '/api/Posts/FriendsPosts';
    private getAllFriendsUrl = API_URL + '/api/Friends/GetAllFriends';
    friendsPosts;
    allFriends;

    constructor(private http: HttpClient, private message: MessageService) {
    }

    getFriendsPosts() {
        return this.http.get(this.getFriendsPostsUrl, httpOptions)
            .pipe(
                tap(p => this.friendsPosts = p, err => this.message.add(err)),
                catchError(this.handleError)
            );
    }

    getAllFriends() {
        return this.http.get(this.getAllFriendsUrl, httpOptions)
            .pipe(
                tap(f => this.allFriends = f, err => this.message.add(err)),
                catchError(this.handleError)
            );
    }


    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(
                `Backend returned code ${error.status}, ` +
                `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError(
            'Something bad happened; please try again later.');
    }
}
