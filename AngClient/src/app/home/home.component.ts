import {Component, OnInit} from '@angular/core';
import {HomeService} from './home.service';
import {UserService} from '../user/user.service';
import {FriendRespondStatus} from '../enums/friendRequestRespondStatus';
import {PostDto} from '../datamodels/PostDto';
import {Observable, zip} from 'rxjs';
import {objectify} from 'tslint/lib/utils';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

    err;
    PostList: Array<PostDto> = new Array<PostDto>();
    public friends;
    public posts;
    public user;

    constructor(private homeService: HomeService, private userService: UserService) {
    }

    makePostData() {
        if (this.posts instanceof Array && this.friends instanceof Array) {
            for (let post of this.posts) {
                if (post.applicationUserId != this.user.applicationUserId) {
                    for (let friend of this.friends) {
                        if (friend.userFriend.applicationUserId == post.applicationUserId) {
                            this.PostList.push(new PostDto(post, friend.userFriend));
                        }
                    }
                } else {
                    this.PostList.push(new PostDto(post, this.user));
                }
            }
        }
    }

    ngOnInit() {
        const example = zip(
            this.homeService.getAllFriends(),
            this.homeService.getFriendsPosts(),
            this.userService.getMyData()
        );
        example.subscribe(([user, friends, posts]) => {
            this.user = user;
            this.friends = friends;
            this.posts = posts;
            if (this.user && this.friends && this.posts) {
                this.makePostData();
            }
        }, err => {
            this.errorAsd(err);
        });
    }

    errorAsd(err) {
        this.err = err;
        console.log(objectify(err));
    }
}
