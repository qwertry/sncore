$(document).ready(function () {
    // SideNav Options
    $('.button-collapse').sideNav({
        edge: 'right', // Choose the horizontal origin
        closeOnClick: true // Closes side-nav on &lt;a&gt; clicks, useful for Angular/Meteor
    });
    // dropdown MDB
    $('.dropdown-toggle').dropdown('toggle');
    // title design MDB
    $('#pt').tooltip();
    $('#ut').tooltip();
    // auth form validation MDB notifications
    $('#LoginFormEmail').change(function () {
        if ($("#LoginFormEmail").val().length < 4) {
            toastr["error"]("the name must contain 4 or more characters!")
        }
        if ($("#LoginFormEmail").val().length == 0) {
            toastr["error"]("the name field is required!")
        }
    });

    $("#LoginFormPassword").change(function () {
        let requiredNum = new RegExp("^(?=.*[0-9])"),
            requiredCharacterLow = new RegExp("^(?=.*[a-z])"),
            requiredCharacterUp = new RegExp("^(?=.*[A-Z])"),
            requiredSymb = new RegExp("^(?=.*[!@#\\$%\\^&\\*])");
        if (!requiredNum.test($('#LoginFormPassword').val())) {
            toastr["error"]("the password must contain at least one numeric character!")
        }
        else if (!requiredCharacterLow.test($('#LoginFormPassword').val())) {
            toastr["error"](" the password must contain at least one lowercase alphabetical character!")
        }
        else if (!requiredCharacterUp.test($('#LoginFormPassword').val())) {
            toastr["error"](" the password must contain at least one uppercase alphabetical character!")
        }
        else if (!requiredSymb.test($('#LoginFormPassword').val())) {
            toastr["error"]("the password must contain at least one special (!@#$%&*) character  character!")
        }
        else if ($('#LoginFormPassword').val().length < 6) {
            toastr["error"]("the password must be six characters or longer!");
        }
    });

    $('#AuthFormEmail').change(function () {
        if ($("#AuthFormEmail").hasClass("is-invalid")) {
            toastr["error"]("the email must contain '@' ex. example@example.com!")
        }
        if ($("#AuthFormEmail").val().length == 0) {
            toastr["error"]("the email field is required!")
        } else {

        }
    });
    $('#AuthFormUserName').change(function () {
        if ($("#AuthFormEmail").val().length < 4) {
            toastr["error"]("the name must contain 4 or more characters!")
        }
        if ($("#AuthFormEmail").val().length == 0) {
            toastr["error"]("the name field is required!")
        }
    });
    $('#AuthFormPassword').change(function () {
        let requiredNum = new RegExp("^(?=.*[0-9])"),
            requiredCharacterLow = new RegExp("^(?=.*[a-z])"),
            requiredCharacterUp = new RegExp("^(?=.*[A-Z])"),
            requiredSymb = new RegExp("^(?=.*[!@#\\$%\\^&\\*])");
        if (!requiredNum.test($('#AuthFormPassword').val())) {
            toastr["error"]("the password must contain at least one numeric character!")
        }
        else if (!requiredCharacterLow.test($('#AuthFormPassword').val())) {
            toastr["error"]("the password must contain at least one lowercase alphabetical character!")
        }
        else if (!requiredCharacterUp.test($('#AuthFormPassword').val())) {
            toastr["error"]("the password must contain at least one uppercase alphabetical character!")
        }
        else if (!requiredSymb.test($('#AuthFormPassword').val())) {
            toastr["error"]("the password must contain at least one special (!@#$%&*) character  character!")
        }
        else if ($('#AuthFormPassword').val().length < 6) {
            toastr["error"]("the password must be six characters or longer!");
        }
    });

});