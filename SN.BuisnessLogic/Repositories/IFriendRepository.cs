﻿
using SN.Domains.Entities;
using SN.Domains.FriendRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.BuisnessLogic.Repositories
{
    public interface IFriendRepository:IRepository<Friend>
    {
        Task<IQueryable<Friend>> GetFriends();
        Task<IQueryable<FriendRequest>> GetFriendRequestsAsync(Guid requestsOwner);
        Task<bool> PlaceRequestAsync(Guid from, Guid to);
        Task<bool> ResponceToFriendRequestAsync(Guid from, ResponceToFriendRequestDto request);
        Task<FriendRequest> GetFriendRequestAsync(Guid from, Guid to);
        Task RemoveFriendRequestAsync(FriendRequest request);
        Task<IQueryable<Friend>> GetAllFriendsAsync(Guid userId);
        Task<bool> IsFriendsAsync(Guid userId, Guid friendId);
        Task DestroyFrindshipAsync(Guid userId, Guid friendId);
    }
}
