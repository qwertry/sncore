﻿using SN.Domains.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace SN.BuisnessLogic.Repositories
{
    public interface IJWTRepository
    {
        object CreateToken(ApplicationUser user);
    }
}
