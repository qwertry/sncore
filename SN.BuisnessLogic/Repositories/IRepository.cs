﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SN.BuisnessLogic.Repositories
{
    public interface IRepository<T> where T : class
    {
        Task AddAsync(T newEntity);
        Task UpdateAsync(T updatableEntity);
        Task RemoveAsync(params T[] entity);
        Task<bool> RecordExistsAsync(Guid recordId);
        Task<IQueryable<T>> FindAsync(Expression<Func<T, bool>> predicate);
        Task<IQueryable<T>> GetAllAsync();
        Task<T> GetByIdAsync(Guid recordId);
        Task<bool> SaveAsync();
    }
}
