﻿using SN.Domains.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SN.BuisnessLogic.Repositories
{
    public interface IHubRepository
    {
        Task SendMessageAsync(Message messageForSend);
    }
}
