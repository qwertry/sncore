﻿using SN.Domains.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.BuisnessLogic.Repositories
{
    public interface IMessageSessionRepository : IRepository<MessageSession> 
    {
        Task<MessageSession> GetMessageSession(Guid applicationUserId, Guid reciever);
        Task<MessageSession> CreateMessageSession(Guid applicationUserId, Guid reciever);
        Task<IQueryable<MessageSession>> GetMessageSessionsOfUser (Guid applicationUserId);
        Task<MessageSession> GetMessageSessionById (Guid messageSessionId);
    }
}
