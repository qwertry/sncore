﻿using SN.Domains.Entities;
using SN.Domains.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.BuisnessLogic.Repositories
{
    public interface IMessageRepository:IRepository<Message>
    {
        Message CreateMessageForSaving(Guid messageSessionId, string messageBody, Guid senderId, Guid recieverId, MessageStatus status);
        Task<Message> FindMessage(Guid messageId);
        Task<bool> ChangeMessageStatus(Message message, MessageStatus newStatus);
        Task<IQueryable<Message>> GetMessages(Guid messagSessionId, int from, int to);

    }
}
