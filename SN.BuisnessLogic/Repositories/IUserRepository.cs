﻿using Microsoft.AspNetCore.Identity;
using SN.Domains.Entities;
using SN.Domains.Models.Inerfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.BuisnessLogic.Repositories
{
    public interface IUserRepository : IRepository<ApplicationUser>
    {
        Task<ApplicationUser> GetUserOrDefaultAsync(ILogable model);
        Task<IdentityResult> CreateAsync(ApplicationUser user, string password);
        Task<IdentityResult> AddToRoleAsync(ApplicationUser user, string role);

    }
}
