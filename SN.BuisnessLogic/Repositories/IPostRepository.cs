﻿using SN.Domains.Entities;
using SN.Domains.Models.RequestDto.Posts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN.BuisnessLogic.Repositories
{
    public interface IPostRepository : IRepository<Post>
    {
        IEnumerable<Post> GetPosts();
        Task<Post> GetPostAsync(Guid userId,int aliasName);
        Task<IQueryable<Post>> GetFriendsPosts(Guid userId);
        Task<IQueryable<Post>> GetFriendsPostsWithMy(Guid userId);
        Task AddPostAsync(PostModelDto post, Guid Owner);
        Task<IEnumerable<Post>> ShowPosts();
        Task UpdatePostAsync(Post postEntity, PostModelDto post);
        Task<bool> IsValid(PostModelDto post, Guid ownerId);
    }
}
