﻿using AutoMapper.QueryableExtensions;
using SN.BuisnessLogic.Repositories;
using SN.Domains.Entities;
using SN.Domains.Enums;
using SN.Domains.Models.RequestDto.Posts;
using SN.Domains.ReturnDtos.PostDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.BuisnessLogic.BuisnesLogic
{
    public class PostsLogic
    {
        public ApplicationUser CurrentUser;
        private IPostRepository _postRepository;
        public PostsLogic(IPostRepository postRepository)
        {
            _postRepository = postRepository;
        }
        public async Task<BuisnessModel> CreateAsync(PostModelDto post)
        {
            Guid ownerId = CurrentUser.Id;

            if (!await _postRepository.IsValid(post, ownerId))
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }

            await _postRepository.AddPostAsync(post, ownerId);

            await _postRepository.SaveAsync();
            return new BuisnessModel() { StatusCode = StatusCode.Ok };
        }
        public async Task<BuisnessModel> EditAsync( PostModelDto post)
        {
            Guid ownerId = CurrentUser.Id;

            if (!await _postRepository.IsValid(post, ownerId))
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }

            var postEntity = await _postRepository.GetPostAsync(ownerId, post.AliasName);

            if (postEntity == null)
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }
            await _postRepository.UpdatePostAsync(postEntity, post);

            await _postRepository.SaveAsync();
            return new BuisnessModel() { StatusCode = StatusCode.Ok };
        }

        public async Task<BuisnessModel> DeleteAsync ( DeletePostModelDto post)
        {
            Guid ownerId = CurrentUser.Id;

            var postEntity = await _postRepository.GetPostAsync(ownerId, post.AliasName);

            if (postEntity == null)
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }
            await _postRepository.RemoveAsync(postEntity);
            await _postRepository.SaveAsync();
            return new BuisnessModel() { StatusCode = StatusCode.Ok };
        }
        public async Task<BuisnessModel> GetFriendsPostsAsync()
        {
            Guid ownerId = CurrentUser.Id;

            var postsOfFriends = await _postRepository.GetFriendsPostsWithMy(ownerId);
            if (postsOfFriends == null)
            {
                return new BuisnessModel() { StatusCode = StatusCode.Ok };
            }
            var json = postsOfFriends.ProjectTo<PostDto>().ToList();
            return new BuisnessModel() { Data = json,  StatusCode = StatusCode.Ok };
        }
    }
}
