﻿using SN.Domains.Models.AuthModels;
using System;
using System.Threading.Tasks;
using SN.Domains.Entities;
using SN.Domains.Enums;
using SN.BuisnessLogic.Repositories;

namespace SN.BuisnessLogic.AuthLogic
{
    public class AuthLogic
    {
        private readonly IJWTRepository _jWTRepository;
        private readonly IUserRepository _userRepository;

        public AuthLogic(IJWTRepository jWTRepository, IUserRepository userRepository)
        {
            _userRepository = userRepository;
            _jWTRepository = jWTRepository;
        }

        public async Task<BuisnessModel> LoginAsync(LoginModelDto model)
        {
            var user = await _userRepository.GetUserOrDefaultAsync(model);
            if (user == null)
            {
                return new BuisnessModel() { StatusCode = StatusCode.Unauthorized };
            }

            return new BuisnessModel() { Data = _jWTRepository.CreateToken(user), StatusCode = StatusCode.Ok };

        }

        public async Task<BuisnessModel> RegisterAsync(RegisterModelDto model)
        {
            var user = await _userRepository.GetUserOrDefaultAsync(model);
            if (user != null)
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest};
            }

            var userForAdd = new ApplicationUser()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.UserName,
                FirstName = model.FirstName,
                SecondName = model.SecondName
            };

            var result = await _userRepository.CreateAsync(userForAdd, model.Password);
            if (!result.Succeeded)
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }

            await _userRepository.AddToRoleAsync(userForAdd, "Member");

            return new BuisnessModel() { Data = _jWTRepository.CreateToken(userForAdd), StatusCode = StatusCode.Ok };

        }

    }
}
