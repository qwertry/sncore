﻿using AutoMapper.QueryableExtensions;
using SN.BuisnessLogic.Repositories;
using SN.Domains.Entities;
using SN.Domains.Enums;
using SN.Domains.FriendRequest;
using SN.Domains.Models.RequestDto.FriendRequest;
using SN.Domains.Models.ReturnDtos.UserDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.BuisnessLogic.FriendsLogic
{
    public class FriendsLogic
    {
        public ApplicationUser CurrentUser { get; set; }
        private IFriendRepository _friendRepository;
        public FriendsLogic(IFriendRepository friendRepository)
        {
            _friendRepository = friendRepository;
        }
        public async Task<BuisnessModel> PlaceFriendRequestAsync(FriendRequestDto request)
        {
            Guid ownerId = CurrentUser.Id;

            if ((ownerId == null || ownerId == Guid.Empty) || (request.ToUser == null || request.ToUser == Guid.Empty) || request.ToUser == ownerId)
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }
            if (await _friendRepository.PlaceRequestAsync(ownerId, request.ToUser))//(from, to)
            {
                await _friendRepository.SaveAsync();

                return new BuisnessModel() { StatusCode = StatusCode.Ok };
            }
            return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
        }

        public async Task<BuisnessModel> DeleteFriendRequestAsync(FriendRequestDto request)
        {
            Guid ownerId = CurrentUser.Id;

            if ((ownerId == null || ownerId == Guid.Empty) || (request.ToUser == null || request.ToUser == Guid.Empty) || request.ToUser == ownerId)
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }

            var friendRequest = await _friendRepository.GetFriendRequestAsync(ownerId, request.ToUser);
            if (friendRequest == null)
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }
            await _friendRepository.RemoveFriendRequestAsync(friendRequest);
            await _friendRepository.SaveAsync();

            return new BuisnessModel() { StatusCode = StatusCode.Ok };

        }

        public async Task<BuisnessModel> DeleteFriendAsync(FriendRequestDto request)
        {

            Guid ownerId = CurrentUser.Id;

            if ((ownerId == null || ownerId == Guid.Empty) || (request.ToUser == null || request.ToUser == Guid.Empty) || request.ToUser == ownerId)
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }

            if (!await _friendRepository.IsFriendsAsync(ownerId, request.ToUser))
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }
            await _friendRepository.DestroyFrindshipAsync(ownerId, request.ToUser);
            await _friendRepository.SaveAsync();

            return  new BuisnessModel() { StatusCode = StatusCode.Ok };

        }

        public async Task<BuisnessModel> ResponceToFriendRequestAsync(ResponceToFriendRequestDto request)
        {

            Guid ownerId = CurrentUser.Id;

            if ((ownerId == null || ownerId == Guid.Empty) || (request.FromUser == null || request.FromUser == Guid.Empty) || request.FromUser == ownerId)
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }
            if (await _friendRepository.ResponceToFriendRequestAsync(ownerId, request))//(from, to)
            {
                await _friendRepository.SaveAsync();
                return new BuisnessModel() { StatusCode = StatusCode.Ok };
            }
            return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
        }

        public async Task<BuisnessModel> GetAllFriendRequestsAsync()
        { 
            var res = await _friendRepository.GetFriendRequestsAsync(CurrentUser.Id);
            var json = res.ProjectTo<SN.Domains.Models.ReturnDtos.RequestDtos.FriendRequestDto>().ToList();
            return new BuisnessModel() { Data = json, StatusCode = StatusCode.Ok };
        }

        public async Task<BuisnessModel> GetAllFriendsAsync()
        {

            var res = await _friendRepository.GetAllFriendsAsync(CurrentUser.Id);

            var json = res.ProjectTo<FriendDto>().ToList();
            return new BuisnessModel() { Data = json, StatusCode = StatusCode.Ok };
        }
    }
}
