﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using SN.BuisnessLogic.Repositories;
using SN.Domains.Entities;
using SN.Domains.Enums;
using SN.Domains.Models.RequestDto.Messages;
using SN.Domains.Models.ReturnDtos.MessageDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN.BuisnessLogic.BuisnesLogic
{
    public class MessagesLogic
    {
        public ApplicationUser CurrentUser;
        private IMessageSessionRepository _messageSessionRepository;
        private IMessageRepository _messageRepository;
        private IUserRepository _userRepository;
        private IHubRepository _hubRepository;

        public MessagesLogic(
            IMessageSessionRepository messageSessionRepository,
            IMessageRepository messageRepository,
            IUserRepository userRepository,
            IHubRepository hubRepository)
        {
            _messageSessionRepository = messageSessionRepository;
            _messageRepository = messageRepository;
            _userRepository = userRepository;
            _hubRepository = hubRepository;
        }

        public async Task<BuisnessModel> SendMessageAsync(MessageToUserDto message)
        {
            Guid ownerId = CurrentUser.Id;

            if (message.ResieverId == ownerId)
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }

            var reciever = await _userRepository.GetByIdAsync(message.ResieverId);
            if (reciever == null)
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }
            var messageSession = await _messageSessionRepository.GetMessageSession(ownerId, message.ResieverId);
            if (messageSession == null)
            {
                messageSession = await _messageSessionRepository.CreateMessageSession(ownerId, message.ResieverId);
                await _messageSessionRepository.SaveAsync();
            }
            var messageForSave = _messageRepository.CreateMessageForSaving(messageSession.MessageSessionId, message.MessageBody, ownerId, reciever.Id, MessageStatus.Sent);
            await _messageRepository.AddAsync(messageForSave);
            if (await _messageRepository.SaveAsync())
            {
                   await _hubRepository.SendMessageAsync(messageForSave);
                return new BuisnessModel() { StatusCode = StatusCode.Ok };
            }
            return new BuisnessModel() { StatusCode = StatusCode.ServerError };
        }

        public async Task<BuisnessModel> GetMessageSessionsAsync()
        {
            Guid ownerId = CurrentUser.Id;

            var userMessageSessions = await _messageSessionRepository.GetMessageSessionsOfUser(ownerId);
            if (!userMessageSessions.Any())
            {
                return new BuisnessModel() { StatusCode = StatusCode.NotFound };
            }
            var responce = userMessageSessions.ProjectTo<MessageSessionDto>().ToList();
            return new BuisnessModel() { Data= responce, StatusCode = StatusCode.Ok };
        }

        public async Task<BuisnessModel> GetMessagesAsync( GetMessagesDto messageModel)
        {
            Guid ownerId = CurrentUser.Id;

            if (messageModel.From < 0 || messageModel.To < 0 || messageModel.From >= messageModel.To)
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }

            var messageSession = await _messageSessionRepository.GetMessageSessionById(messageModel.MessageSessionId);
            if (messageSession == null)
            {
                return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
            }

            if (messageSession.ApplicationUserId == ownerId || messageSession.RecieverId == ownerId)
            {
                var messages = await _messageRepository.GetMessages(messageModel.MessageSessionId, messageModel.From, messageModel.To);
                if (!messages.Any())
                {
                    return new BuisnessModel() { StatusCode = StatusCode.NotFound };
                }

                var responce = messages.ProjectTo<MessageDto>().ToList();
                
                return new BuisnessModel() { Data = responce, StatusCode = StatusCode.BadRequest };
            }
            return new BuisnessModel() { StatusCode = StatusCode.BadRequest };
        }
    }
}