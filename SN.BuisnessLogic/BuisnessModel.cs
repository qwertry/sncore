﻿using SN.Domains.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace SN.BuisnessLogic
{
    public class BuisnessModel
    {
        public object Data { get; set; }
        public StatusCode StatusCode { get; set; }
    }
}
