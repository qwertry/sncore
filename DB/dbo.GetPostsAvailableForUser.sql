﻿DROP PROCEDURE [GetPostsAvailableForUser]
USE [SN_DB]
GO

CREATE PROCEDURE [dbo].[GetPostsAvailableForUser]
	@Id uniqueidentifier
AS
	SELECT * FROM Post AS P 
	JOIN Friend  AS F  ON P.ApplicationUserId = F.UserFriendId AND F.UserId = @Id
	WHERE  P.PostPermissionType = 0 OR P.PostPermissionType = 1
		OR (P.PostPermissionType = 2 AND NOT EXISTS ( SELECT * FROM [PermitedUsers] as PU WHERE PU.PostId = P.PostId AND PU.ApplicationUserId=@Id) )
		OR (P.PostPermissionType = 3 AND EXISTS ( SELECT * FROM [PermitedUsers] as PU WHERE PU.PostId = P.PostId AND PU.ApplicationUserId=@Id) )
	ORDER BY P.PostedAt DESC
RETURN 0
